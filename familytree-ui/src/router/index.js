import {
	createRouter,
	createWebHashHistory
} from 'vue-router'
//共用路由
import Login from '../components/login.vue'
import Home from '../components/home.vue'
import Welcome from '../components/welcome.vue'
import Nf from '../components/noFound.vue'
//管理员
import family from '../components/admin/family.vue'
import member from '../components/admin/member.vue'
import number from '../components/admin/number.vue'
import relation from '../components/admin/relation.vue'
import familytree from '../components/admin/familytree.vue'
import resume from '../components/admin/resume.vue'
<<<<<<< HEAD
=======
import visualization from '../components/admin/visualization.vue'
>>>>>>> 2e4c08b (逻辑删除bug已修改)
//用户
import family_tree from '../components/common/family_tree.vue'
import familymessage from '../components/common/familymessage.vue'
import membermessage from '../components/common/membermessage.vue'
import relation_ from '../components/common/relation_.vue'
import resume_ from '../components/common/resume_.vue'

const routes = [


	{
		path: '/login',
		component: Login,
		meta: {
			title: "登录"
		},
	},
	{
		path: '/home',
		component: Home,
		meta: {
			title: "主页"
		},
		redirect: '/welcome',
		children: [{
			path: '/welcome',
			component: Welcome,
		},
		{
			path: '/family',
			component: family,
		},
		{
			path: '/member',
			component: member,
		},
		{
			path: '/number',
			component: number,
		},
		{
			path: '/familytree',
			component: familytree,
		},

		{
			path: '/familymessage',
			component: familymessage,
		},
		{
			path: '/membermessage',
			component: membermessage,
		},
		{
			path: '/family_tree',
			component: family_tree,
		},
		{
			path: '/relation',
			component: relation,
		},
		{
			path: '/resume',
			component: resume,
		},
		{
			path: '/relation_',
			component: relation_,
		},
		{
			path: '/resume_',
			component: resume_,
<<<<<<< HEAD
=======
		},
		{
			path: '/visualization',
			component: visualization,
>>>>>>> 2e4c08b (逻辑删除bug已修改)
		}

		],
	},
	{
		path: "/404",
		component: Nf,
		meta: {
			title: "404"
		},
	},
	{
		path: "/:catchAll(.*)",
		redirect: "/404"
	},
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
	if (to.meta.title) {
		document.title = to.meta.title;
	}
	if (to.path == '/login') return next(); // 访问路径为登录
	// 获取flag
	const flagStr = window.sessionStorage.getItem("username"); // session取值
	if (!flagStr) return next('/login'); // 没登录去登录
	next();
})

export default router
