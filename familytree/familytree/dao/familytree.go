package dao

import "familytree/models"

func GetFamilytree() (error, []models.Visualization, int64) {
	var familytreeData []models.Visualization

	var total int64

	err := db.Table("visualization").Select("id,pids,mid,fid,name,gender,img").Order("id ASC").Count(&total).Find(&familytreeData).Error

	return err, familytreeData, total
}
