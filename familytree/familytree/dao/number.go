package dao

import "familytree/models"

func GetNumbers(student map[string]interface{}) (error, []models.Number, int64) {
	var numberDate []models.Number
	var total int64
	err := db.Table("member").Select("member_family, COUNT(member_family)").Group("member_family").Count(&total).Find(&numberDate).Error
	return err, numberDate, total
}
