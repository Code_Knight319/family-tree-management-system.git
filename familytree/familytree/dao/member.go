package dao

import "familytree/models"

func GetAllMember(member map[string]interface{}) (error, []models.Member, int64) {
	var memberData []models.Member
	page := member["page"].(int)
	pageSize := member["limit"].(int)
	searchName := member["searchName"].(string)
	var total int64
	err := db.Table("member").Select("member_tel, member_name, member_sex, member_age, member_family, father, mother, partner, children, member_email, member_country, member_city, member_resume, update_time").Where("member_name like ? And is_del = false", searchName+"%").Order("member_tel ASC").Count(&total).Offset((page - 1) * pageSize).Limit(pageSize).Find(&memberData).Error
	return err, memberData, total
}
func DelMember(id string) error {
	err := db.Table("member").Where("member_tel = ?", id).Update("is_del", true).Error
	return err
}
func UpdateMember(member models.Member) error {
	err := db.Table("member").Where("member_tel = ?", member.Member_tel).Updates(&member).Error
	return err
}
func AddMember(member models.Member) error {
	err := db.Table("member").Select("member_tel", "member_name", "member_sex", "member_age", "member_family", "father", "mother", "partner", "children", "member_email", "member_country", "member_city", "member_resume", "update_time", "is_del").Create(&member).Error
	return err
}
