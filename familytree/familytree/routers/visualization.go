package routers

import (
	"familytree/controllers"

	"github.com/gin-gonic/gin"
)

func VisualRouter(r *gin.RouterGroup) {
	r.GET("/visualization", controllers.GetVisual)
	r.DELETE("/visualization", controllers.DelVisual)
	r.PUT("/visualization", controllers.UpdateVisual)
	r.POST("/visualization", controllers.Addvisual)
}
