package routers

import (
	"familytree/controllers"

	"github.com/gin-gonic/gin"
)

func NumberRouter(r *gin.RouterGroup) {
	r.GET("/number", controllers.GetNumbers)
}
