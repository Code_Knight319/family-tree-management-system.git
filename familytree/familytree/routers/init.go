package routers

import (
	"familytree/middleWare"

	"github.com/gin-gonic/gin"
)

func sysNoCheckRoleRouter(r *gin.RouterGroup) {
	r = r.Group("/apis")
	loginRouter(r)
	menuRouter(r)
	FamilyRouter(r)
	memberRouter(r)
	NumberRouter(r)
	VisualRouter(r)
	FamilytreeRouter(r)
}

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(middleWare.Cors())
	g := r.Group("/system")
	sysNoCheckRoleRouter(g)
	return r
}
