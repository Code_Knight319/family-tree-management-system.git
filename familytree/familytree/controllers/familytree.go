package controllers

import (
	"familytree/pkg/app"
	"familytree/pkg/e"
	"familytree/services"

	"github.com/gin-gonic/gin"
)

func GetFamilytree(c *gin.Context) {

	err, info, total := services.GetFamilytree()

	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	var m = map[string]interface{}{"value": info, "total": total}

	app.OK(c, m, "查询成功")
}
