package controllers

import (
	"encoding/json"
	"familytree/models"
	"familytree/pkg/app"
	"familytree/pkg/e"
	"familytree/services"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func GetVisual(c *gin.Context) {
	page := -1
	if arg := c.Query("page"); arg != "" {
		page = com.StrTo(arg).MustInt()
	}
	limit := -1
	if arg := c.Query("limit"); arg != "" {
		limit = com.StrTo(arg).MustInt()
	}
	searchName := ""
	if arg := c.Query("searchName"); arg != "" {
		searchName = arg
	}
	supplierParam := map[string]interface{}{
		"page":       page,
		"limit":      limit,
		"searchName": searchName,
	}
	err, info, total := services.GetAllVisual(supplierParam)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{"value": info, "total": total}, "查询成功")
}

func DelVisual(c *gin.Context) {
	id := ""
	if arg := c.Query("id"); arg != "" {
		id = arg
	}
	if id == "" {
		app.INFO(c, 30001, "参数错误")
		return
	}
	err := services.DelVisual(id)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "删除成功")
}

func UpdateVisual(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	if m["Id"] == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	Id := com.StrTo(m["Id"]).MustInt()
	Pids := com.StrTo(m["Pids"]).MustInt()
	Mid := com.StrTo(m["Mid"]).MustInt()
	Fid := com.StrTo(m["Fid"]).MustInt()
	Name := m["Name"]
	Gender := m["Gender"]
	Birth := m["Birth"]
	Familytree := m["Familytree"]
	Img := m["Img"]
	Tel := m["Tel"]
	Email := m["Email"]
	Resume := m["Resume"]
	err := services.UpdateVisual(models.Visualization{Id: Id, Pids: Pids, Mid: Mid, Fid: Fid, Name: Name, Gender: Gender, Birth: Birth, Familytree: Familytree, Img: Img, Tel: Tel, Email: Email, Resume: Resume, Isdel: false, UpdateTime: time.Now()})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "更新成功")
}

func Addvisual(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	Id := com.StrTo(m["Id"]).MustInt()
	Pids := com.StrTo(m["Pids"]).MustInt()
	Mid := com.StrTo(m["Mid"]).MustInt()
	Fid := com.StrTo(m["Fid"]).MustInt()
	Name := m["Name"]
	Gender := m["Gender"]
	Birth := m["Birth"]
	Familytree := m["Familytree"]
	Img := m["Img"]
	Tel := m["Tel"]
	Email := m["Email"]
	Resume := m["Resume"]
	err := services.AddVisual(models.Visualization{Id: Id, Pids: Pids, Mid: Mid, Fid: Fid, Name: Name, Gender: Gender, Birth: Birth, Familytree: Familytree, Img: Img, Tel: Tel, Email: Email, Resume: Resume, Isdel: false, UpdateTime: time.Now()})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "添加成功")
}
