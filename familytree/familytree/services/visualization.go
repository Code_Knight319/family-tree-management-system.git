package services

import (
	"familytree/dao"
	"familytree/models"
)

func GetAllVisual(visual map[string]interface{}) (error, []models.Visualization, int64) {
	err, visualData, total := dao.GetAllVisual(visual)
	return err, visualData, total
}

func DelVisual(id string) error {
	err := dao.DelVisual(id)
	return err
}

func UpdateVisual(visual models.Visualization) error {
	err := dao.UpdateVisual(visual)
	return err
}

func AddVisual(visual models.Visualization) error {
	err := dao.Addvisual(visual)
	return err
}
