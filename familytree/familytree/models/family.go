package models

import "time"

type Family struct {
	Header_tel         string `gorm:"column:header_tel"`
	Family_declaration string `gorm:"column:family_declaration"`
	Header_name        string `gorm:"column:header_name"`
	Family_name        string `gorm:"column:family_name"`
	Isdel              bool
	UpdateTime         time.Time
}
