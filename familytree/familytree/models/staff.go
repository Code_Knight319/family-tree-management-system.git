package models

import "time"

type Staff struct {
	StaffId       int       `gorm:"column:staff_id"`
	StaffName     string    `grom:"column:staff_name"`
	StaffPassword string    `grom:"column:staff_password"`
	StaffLevel    int       `grom:"column:staff_level"`
	StaffRemarks  string    `grom:"column:staff_remarks"`
	Isdel         bool      `gorm:"column:is_del"`
	UpdateTime    time.Time `gorm:"column:update_time"`
}
