package models

type Number struct {
	Member_family  string `gorm:"column:member_family"`
	Member_email   string `gorm:"column:member_email"`
	Member_country string `gorm:"column:member_country"`
	Count          int    `gorm:"colum:COUNT"`
}
