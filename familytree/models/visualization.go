package models

import "time"

type Visualization struct {
	Id         int       `gorm:"column:id"`
	Pids       int       `gorm:"column:pids"`
	Mid        int       `gorm:"column:mid"`
	Fid        int       `gorm:"column:fid"`
	Name       string    `gorm:"column:name"`
	Gender     string    `gorm:"column:gender"`
	Birth      string    `gorm:"column:birthday"`
	Familytree string    `gorm:"column:familytree"`
	Img        string    `gorm:"column:img"`
	Tel        string    `gorm:"column:tel"`
	Email      string    `gorm:"column:email"`
	Resume     string    `gorm:"column:resume"`
	Isdel      bool      `gorm:"column:is_del"`
	UpdateTime time.Time `gorm:"column:update_time"`
}
