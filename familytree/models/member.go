package models

import "time"

type Member struct {
	Member_tel    string `gorm:"column:member_tel"`
	Member_name   string `gorm:"column:member_name"`
	Member_sex    string `gorm:"column:member_sex"`
	Member_family string `gorm:"column:member_family"`

	Father   string `gorm:"column:father"`
	Mother   string `gorm:"column:mother"`
	Partner  string `gorm:"column:partner"`
	Children string `gorm:"column:children"`

	Member_email   string `gorm:"column:member_email"`
	Member_age     int    `gorm:"column:member_age"`
	Member_country string `gorm:"column:member_country"`
	Member_city    string `gorm:"column:member_city"`
	Member_resume  string `gorm:"column:member_resume"`
	Isdel          bool
	UpdateTime     time.Time
}
