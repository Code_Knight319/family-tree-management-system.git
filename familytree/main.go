package main

import (
	"familytree/dao"
	"familytree/pkg/gredis"
	"familytree/pkg/setting"
	"familytree/routers"
	"fmt"
)

//pg数据库初始化，gedis初始化
func init() {
	setting.Setup()
	dao.Setup()
	gredis.Setup()
}
func main() {
	endPoint := fmt.Sprintf(":%d", setting.ServerSetting.HttpPort)
	routersInit := routers.InitRouter()
	routersInit.Run(endPoint) //开启监听
}

//
