package controllers

import (
	"encoding/json"
	"familytree/models"
	"familytree/pkg/app"
	"familytree/pkg/e"
	"familytree/services"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func GetMember(c *gin.Context) {
	page := -1
	if arg := c.Query("page"); arg != "" {
		page = com.StrTo(arg).MustInt()
	}
	limit := -1
	if arg := c.Query("limit"); arg != "" {
		limit = com.StrTo(arg).MustInt()
	}
	searchName := ""
	if arg := c.Query("searchName"); arg != "" {
		searchName = arg
	}
	supplierParam := map[string]interface{}{
		"page":       page,
		"limit":      limit,
		"searchName": searchName,
	}
	err, info, total := services.GetAllMember(supplierParam)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{"value": info, "total": total}, "查询成功")
}

func DelMember(c *gin.Context) {
	id := ""
	if arg := c.Query("member_tel"); arg != "" {
		id = arg
	}
	if id == "" {
		app.INFO(c, 30001, "参数错误")
		return
	}
	err := services.DelMember(id)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "删除成功")
}
func UpdateMember(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	if m["Member_tel"] == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	Member_tel := m["Member_tel"]
	Member_name := m["Member_name"]
	Member_email := m["Member_email"]
	Member_sex := m["Member_sex"]
	Member_family := m["Member_family"]

	Father := m["Father"]
	Mother := m["Mother"]
	Partner := m["Partner"]
	Children := m["Children"]

	Member_age := com.StrTo(m["Member_age"]).MustInt()
	Member_country := m["Member_country"]
	Member_city := m["Member_city"]
	Member_resume := m["Member_resume"]
	err := services.UpdateMember(models.Member{Member_tel: Member_tel, Member_name: Member_name, Member_sex: Member_sex, Member_family: Member_family, Father: Father, Mother: Mother, Partner: Partner, Children: Children, Member_email: Member_email, Member_age: Member_age, Member_country: Member_country, Member_city: Member_city, Isdel: false, Member_resume: Member_resume, UpdateTime: time.Now()})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "更新成功")
}

func AddMember(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	Member_tel := m["Member_tel"]
	Member_name := m["Member_name"]
	Member_email := m["Member_email"]
	Member_sex := m["Member_sex"]
	Member_family := m["Member_family"]

	Father := m["Father"]
	Mother := m["Mother"]
	Partner := m["Partner"]
	Children := m["Children"]

	Member_age := com.StrTo(m["Member_age"]).MustInt()
	Member_country := m["Member_country"]
	Member_city := m["Member_city"]
	Member_resume := m["Member_resume"]
	err := services.AddMember(models.Member{Member_tel: Member_tel, Member_name: Member_name, Member_sex: Member_sex, Member_family: Member_family, Father: Father, Mother: Mother, Partner: Partner, Children: Children, Member_email: Member_email, Member_age: Member_age, Member_country: Member_country, Member_city: Member_city, Isdel: false, Member_resume: Member_resume, UpdateTime: time.Now()})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "添加成功")
}
