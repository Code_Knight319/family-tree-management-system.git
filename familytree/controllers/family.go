package controllers

import (
	"encoding/json"
	"familytree/models"
	"familytree/pkg/app"
	"familytree/pkg/e"
	"familytree/services"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func GetFamily(c *gin.Context) {
	page := -1
	if arg := c.Query("page"); arg != "" {
		page = com.StrTo(arg).MustInt()
	}
	limit := -1
	if arg := c.Query("limit"); arg != "" {
		limit = com.StrTo(arg).MustInt()
	}
	searchName := ""
	if arg := c.Query("searchName"); arg != "" {
		searchName = arg
	}
	supplierParam := map[string]interface{}{
		"page":       page,
		"limit":      limit,
		"searchName": searchName,
	}
	err, info, total := services.GetAllFamily(supplierParam)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{"value": info, "total": total}, "查询成功")
}
func DelFamily(c *gin.Context) {
	id := ""
	if arg := c.Query("header_tel"); arg != "" {
		id = arg
	}
	if id == "" {
		app.INFO(c, 30001, "参数错误")
		return
	}
	err := services.DelFamily(id)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "删除成功")
}
func Updatefamily(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	if m["Family_name"] == "" {
		app.INFO(c, 30000, "参数非法")
		return
	}
	headertel := m["Header_tel"]
	familydeclaration := m["Family_declaration"]
	headername := m["Header_name"]
	familyname := m["Family_name"]
	err := services.UpdateFamily(models.Family{Header_tel: headertel, Family_declaration: familydeclaration, Header_name: headername, Family_name: familyname, Isdel: false, UpdateTime: time.Now()})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "更新成功")
}
func AddFamily(c *gin.Context) {
	b, _ := c.GetRawData()
	var m map[string]string
	_ = json.Unmarshal(b, &m)
	headertel := m["Header_tel"]
	familydeclaration := m["Family_declaration"]
	headername := m["Header_name"]
	familyname := m["Family_name"]
	err := services.AddFamily(models.Family{Header_tel: headertel, Family_declaration: familydeclaration, Header_name: headername, Family_name: familyname, Isdel: false, UpdateTime: time.Now()})
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{}, "添加成功")
}
