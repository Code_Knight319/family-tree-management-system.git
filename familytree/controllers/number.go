package controllers

import (
	"familytree/pkg/app"
	"familytree/pkg/e"
	"familytree/services"

	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
)

func GetNumbers(c *gin.Context) {
	page := -1
	if arg := c.Query("page"); arg != "" {
		page = com.StrTo(arg).MustInt()
	}
	limit := -1
	if arg := c.Query("limit"); arg != "" {
		limit = com.StrTo(arg).MustInt()
	}
	supplierParam := map[string]interface{}{
		"page":  page,
		"limit": limit,
	}
	err, info, total := services.GetNumbers(supplierParam)
	if err != nil {
		app.Error(c, e.ERROR, err, err.Error())
		return
	}
	app.OK(c, map[string]interface{}{"value": info, "total": total}, "查询成功")
}
