package services

import (
	"familytree/dao"
	"familytree/models"
)

func GetAllMember(member map[string]interface{}) (error, []models.Member, int64) {
	err, memberData, total := dao.GetAllMember(member)
	return err, memberData, total
}
func DelMember(id string) error {
	err := dao.DelMember(id)
	return err
}
func UpdateMember(member models.Member) error {
	err := dao.UpdateMember(member)
	return err
}
func AddMember(member models.Member) error {
	err := dao.AddMember(member)
	return err
}
