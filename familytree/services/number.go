package services

import (
	"familytree/dao"
	"familytree/models"
)

func GetNumbers(members map[string]interface{}) (error, []models.Number, int64) {
	err, numberData, total := dao.GetNumbers(members)
	return err, numberData, total
}
