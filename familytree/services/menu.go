package services

import (
	"familytree/dao"
	"familytree/models"
)

func GetMenuByLevel(level int) (error, []models.Menu) {
	err, menuData := dao.GetMenuByLevel(level)
	return err, menuData
}
