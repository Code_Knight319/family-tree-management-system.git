package services

import (
	"familytree/dao"
	"familytree/models"
)

func GetAllFamily(family map[string]interface{}) (error, []models.Family, int64) {
	err, familyData, total := dao.GetAllFamily(family)
	return err, familyData, total
}
func DelFamily(id string) error {
	err := dao.DelFamily(id)
	return err
}
func UpdateFamily(family models.Family) error {
	err := dao.UpdateFamily(family)
	return err
}
func AddFamily(family models.Family) error {
	err := dao.AddFamily(family)
	return err
}
