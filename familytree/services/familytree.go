package services

import (
	"familytree/dao"
	"familytree/models"
)

func GetFamilytree() (error, []models.Visualization, int64) {
	err, familytreeData, total := dao.GetFamilytree()
	return err, familytreeData, total
}
