package dao

import "familytree/models"

func GetAllVisual(visual map[string]interface{}) (error, []models.Visualization, int64) {
	var visualData []models.Visualization
	page := visual["page"].(int)
	pageSize := visual["limit"].(int)
	searchName := visual["searchName"].(string)
	var total int64
	err := db.Table("visualization").Select("*").Where("name like ? And is_del = false", searchName+"%").Order("id ASC").Count(&total).Offset((page - 1) * pageSize).Limit(pageSize).Find(&visualData).Error
	return err, visualData, total
}

func DelVisual(id string) error {
	err := db.Table("visualization").Where("id = ?", id).Update("is_del", true).Error
	return err
}

func UpdateVisual(visual models.Visualization) error {
	err := db.Table("visualization").Where("id = ?", visual.Id).Updates(&visual).Error
	return err
}

func Addvisual(visual models.Visualization) error {
	err := db.Table("visualization").Select("id", "pids", "mid", "fid", "name", "gender", "birthday", "familytree", "img", "tel", "email", "resume", "update_time", "is_del").Create(&visual).Error
	return err
}
