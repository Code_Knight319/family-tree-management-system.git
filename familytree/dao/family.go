package dao

import "familytree/models"

func GetAllFamily(family map[string]interface{}) (error, []models.Family, int64) {
	var familyData []models.Family
	page := family["page"].(int)
	pageSize := family["limit"].(int)
	searchName := family["searchName"].(string)
	var total int64
	err := db.Table("family").Select("header_tel,family_declaration,family_name, header_name, update_time").Where("family_name like ? and is_del = false", searchName+"%").Order("header_tel ASC").Count(&total).Offset((page - 1) * pageSize).Limit(pageSize).Find(&familyData).Error
	return err, familyData, total
}
func DelFamily(id string) error {
	err := db.Table("family").Where("header_tel = ?", id).Update("is_del", true).Error
	return err
}
func UpdateFamily(family models.Family) error {
	err := db.Table("family").Select("header_tel", "update_time", "family_declaration", "header_name").Where("family_name = ?", family.Family_name).Updates(&family).Error
	return err
}
func AddFamily(family models.Family) error {
	err := db.Table("family").Select("family_name", "header_tel", "update_time", "family_declaration", "header_name", "is_del").Create(&family).Error
	return err
}
