package routers

import (
	"familytree/controllers"

	"github.com/gin-gonic/gin"
)

func FamilyRouter(r *gin.RouterGroup) {
	r.GET("/family", controllers.GetFamily)
	r.DELETE("/family", controllers.DelFamily)
	r.PUT("/family", controllers.Updatefamily)
	r.POST("/family", controllers.AddFamily)
}
