package routers

import (
	"familytree/controllers"

	"github.com/gin-gonic/gin"
)

func FamilytreeRouter(r *gin.RouterGroup) {
	r.GET("/familytree", controllers.GetFamilytree)
}
