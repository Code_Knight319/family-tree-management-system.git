package routers

import (
	"familytree/controllers"

	"github.com/gin-gonic/gin"
)

func memberRouter(r *gin.RouterGroup) {
	r.GET("/member", controllers.GetMember)
	r.DELETE("/member", controllers.DelMember)
	r.PUT("/member", controllers.UpdateMember)
	r.POST("/member", controllers.AddMember)
}
