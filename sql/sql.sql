
DROP TABLE IF EXISTS "public"."member";
CREATE TABLE "public"."member" (
  "member_tel" varchar(11) primary key,
  "member_name" varchar(50) COLLATE "pg_catalog"."default",
  "member_sex" varchar(2) COLLATE "pg_catalog"."default",
  "member_age" int NOT NULL,
  "member_family" varchar(100) COLLATE "pg_catalog"."default",
  "father" varchar(255),
  "mother" varchar(255),
  "partner" varchar(255),
  "children" varchar(255),
  "member_email" varchar(100) NOT NULL,
  "member_country" varchar(100) COLLATE "pg_catalog"."default",
  "member_city" varchar(100) COLLATE "pg_catalog"."default",
  "member_resume" varchar(255)  COLLATE "pg_catalog"."default",
  "is_del" bool NOT NULL DEFAULT false,
  "update_time" timestamptz(6) NOT NULL
);
INSERT INTO "public"."member" VALUES('123123','张小荣','男','30','霸王别姬','张父','张母','张妻','张子、张女','12321@qq.com','中国','北京','不疯魔不成活', 'f', '2022-06-08 08:00:00+08');


DROP TABLE IF EXISTS "public"."family";
CREATE TABLE "public"."family"(
 "header_tel" varchar(11) primary key,
 "change_time" date,
 "family_declaration" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
 "header_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
 "family_name" varchar(100) COLLATE "pg_catalog"."default",
  "is_del" bool NOT NULL DEFAULT false,
  "update_time" timestamptz(6) NOT NULL
);
INSERT INTO "public"."family" VALUES('123123','2022-06-08','不疯魔不成活','张小荣','不疯魔不成活', 'f', '2021-06-20 08:00:00+08');


DROP TABLE IF EXISTS "public"."staff";
CREATE TABLE "public"."staff" (
  "staff_id" int primary key,
  "staff_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "staff_password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "staff_level" int NOT NULL,
  "staff_remarks" varchar(255) COLLATE "pg_catalog"."default",
  "is_del" bool NOT NULL DEFAULT false,
  "update_time" timestamptz(0) NOT NULL
);
INSERT INTO staff VALUES (1, 'admin', 'admin', 2,  '管理员', 'f', '2021-06-20 11:04:59+08');
INSERT INTO staff VALUES (2, '111111', '111111', 1, '普通用户', 'f', '2021-06-20 11:06:24+08');


DROP TABLE IF EXISTS "public"."menu";
CREATE TABLE "public"."meu"(
  "menu_id" int4 NOT NULL,
  "menu_address" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "menu_level" int4 NOT NULL,
  "menu_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
);
INSERT INTO menu VALUES(1,'/familytree',2,'制作家谱');
INSERT INTO menu VALUES(2,'/number',2,'统计人数');
INSERT INTO menu VALUES(3,'/family',2,'家族管理');
INSERT INTO menu VALUES(4,'/member',2,'成员管理');
INSERT INTO menu VALUES(5,'/relation',2,'成员关系');
INSERT INTO menu VALUES(6,'/resume',2,'成员履历');
INSERT INTO menu VALUES(7,'/familymessage',1,'家族信息');
INSERT INTO menu VALUES(8,'/membermessage',1,'成员信息');
INSERT INTO menu VALUES(9,'/family_tree',1,'绘制家谱');
INSERT INTO menu VALUES(10,'/relation_',1,'成员关系');
INSERT INTO menu VALUES(11,'/resume_',1,'成员履历');
























