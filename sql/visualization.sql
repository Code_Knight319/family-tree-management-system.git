
DROP TABLE IF EXISTS "public"."visualization";
CREATE TABLE "public"."visualization" (
  "id" int primary key,
  "pids" int,
  "mid" int,
  "fid" int,
  "name" varchar(255),
  "gender" varchar(255),
  "birthday" varchar(255),
  "familytree" varchar(255),
  "img" varchar(255),
  "tel" varchar(255),
  "email" varchar(255),
  "resume" varchar(255),
  "is_del" bool NOT NULL DEFAULT false,
  "update_time" timestamptz(6) NOT NULL
);
/*
INSERT INTO "public"."visualization" (id,pids,mid,fid,name,gender,birthday,familytree,img,tel,email,resume,is_del,update_time)
VALUES('1','0','0','0','蒋介石','男','1887年10月31日-1975年4月5日','蒋介石家族','1.jpg','123456','123456@tel.com','原名瑞元，学名志清、中正，字介石。', 'f', '2022-06-08 08:00:00+08');
*/